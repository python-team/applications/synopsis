int foo(float x)
{
  __extension__ union { float f; int i; } u = { .f = x };
  return u.i < 0;
}
