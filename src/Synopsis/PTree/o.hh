#include <cassert>

template <typename N> N *nconc(N *p, Node *q) 
{
  assert(p);
  last(p)->set_cdr(q);
  return p;
}

Node *snoc(Node *, Node *);
template <typename N> N *snoc(N *p, Node *q)
{
  return nconc(p, cons(q, 0));
}
