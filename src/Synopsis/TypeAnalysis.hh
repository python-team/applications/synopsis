//
// Copyright (C) 2005 Stefan Seefeld
// All rights reserved.
// Licensed to the public under the terms of the GNU LGPL (>= 2),
// see the file COPYING for details.
//
#ifndef Synopsis_TypeAnalysis_hh_
#define Synopsis_TypeAnalysis_hh_

#include <Synopsis/TypeAnalysis/Type.hh>
#include <Synopsis/TypeAnalysis/Visitor.hh>
#include <Synopsis/TypeAnalysis/Kit.hh>
#include <Synopsis/TypeAnalysis/TypeEvaluator.hh>
#include <Synopsis/TypeAnalysis/ConstEvaluator.hh>
#include <Synopsis/TypeAnalysis/OverloadResolver.hh>

#endif
