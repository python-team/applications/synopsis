<sxr filename="Polyline.h">
<line>#ifndef Polyline_h_</line>
<line>#define Polyline_h_</line>
<line></line>
<line>#include "Path.h"</line>
<line>#include &lt;vector&gt;</line>
<line></line>
<line><span class="keyword">namespace</span> <a href="Paths" title="namespace Paths" from="Paths" type="definition">Paths</a></line>
<line>{</line>
<line></line>
<line><span class="comment">// The Polyline class. It is an ordered set of</span></line>
<line><span class="comment">// connected line segments.</span></line>
<line><span class="keyword">class</span> <a href="Paths::Polyline" title="class Paths::Polyline" from="Paths::Polyline" type="definition">Polyline</a> : <span class="keyword">public</span> <a href="Path" title="class Path" from="Paths::Polyline" type="reference">Path</a></line>
<line>{</line>
<line><span class="keyword">public</span>:</line>
<line>  <span class="comment">// Create a new Polyline.</span></line>
<line><span class="comment">  //</span></line>
<line>  <a href="Paths::Polyline::Polyline()" title="member function Paths::Polyline::Polyline()" from="Paths::Polyline" type="definition">Polyline</a>();</line>
<line>  <span class="comment">// @group Manipulators {</span></line>
<line></line>
<line>  <span class="comment">// Add a new vertex.</span></line>
<line>  <span class="keyword">void</span> <a href="Paths::Polyline::add_vertex(const Vertex&amp;)" title="member function Paths::Polyline::add_vertex(const Vertex&amp;)" from="Paths::Polyline" type="definition">add_vertex</a>(<span class="keyword">const</span> <a href="Vertex" title="struct Vertex" from="Paths::Polyline" type="reference">Vertex</a> &amp;);</line>
<line>  <span class="comment">// Remove the vertex at index i.</span></line>
<line>  <span class="keyword">void</span> <a href="Paths::Polyline::remove_vertex(size_t)" title="member function Paths::Polyline::remove_vertex(size_t)" from="Paths::Polyline" type="definition">remove_vertex</a>(<a href="size_t" title="typedef size_t" from="Paths::Polyline" type="reference">size_t</a> i);</line>
<line>  <span class="comment">// }</span></line>
<line>  virtual <span class="keyword">void</span> <a href="Paths::Polyline::draw()" title="member function Paths::Polyline::draw()" from="Paths::Polyline" type="definition">draw</a>();</line>
<line><span class="keyword">private</span>:</line>
<line>  <span class="comment">// The data...</span></line>
<line>  std::<a href="std::vector" title="class std::vector" from="Paths::Polyline" type="reference">vector</a>&lt;<a href="Vertex" title="struct Vertex" from="Paths::Polyline" type="reference">Vertex</a>&gt; <a href="Paths::Polyline::vertices_" title="data member Paths::Polyline::vertices_" from="Paths::Polyline" type="definition">vertices_</a>;</line>
<line>};</line>
<line></line>
<line>}</line>
<line></line>
<line>#endif</line>
</sxr>